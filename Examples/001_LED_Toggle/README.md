﻿**001_LED_Toggle**

This project provides a basic template for programming STM32 microcontrollers to toggle LEDs.

**Overview**

The main.c file contains the main program body for the STM32 microcontroller project. It sets up the microcontroller to toggle LEDs connected to GPIO pins in an infinite loop.

**Features**

- **LED Toggle Functionality**: The project includes a custom function called 'led_toggle()' (located in led_toggle.c file) for toggling LEDs connected to GPIO pins.
- **LED Toggle function Description:** This function toggles three** LEDs (blue, green, and red) connected to GPIO pins on the STM32 microcontroller. It creates a cycling pattern of LED toggling, where each LED is toggled sequentially with a delay of 250 milliseconds.


