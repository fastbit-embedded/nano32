002_UserButton_LED

This project contains the implementation of function to control an LED based on the state of a user button.

## Overview
The �user_button_led ( )� function (located in the �user_button_led.c� file) ,  initializes GPIO pins for the user button and LED. It continuously reads the state of the user button to control the LED accordingly.

## Functionality
* Control LED: the function reads the state of the user button. If the button is pressed, it turns on the LED. If the button is released, it turns off the LED.
* Debounce Mechanism: A short delay (50 milliseconds) is included to debounce the switch, ensuring reliable button state detection.
